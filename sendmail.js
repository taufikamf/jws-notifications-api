var nodemailer = require('nodemailer');
require("dotenv").config()

var transporter = nodemailer.createTransport({
    host: process.env.MAILER_HOST,
    port: process.env.MAILER_PORT,
    secure: process.env.MAILER_SECURE,
    auth : {
        user : process.env.MAILER_USER,
        pass : process.env.MAILER_PASS
    }
});

var mailOptions = {
    from: 'youremail@gmail.com',
    to: 'taufikabdul354@gmail.com',
    subject: 'Sending Email using Nodejs',
    text: 'That was easy!'
};

transporter.sendMail(mailOptions, (err, info) => {
    if (err) throw err;
    console.log('Email sent: ' + info.response);
});