const dbConfig = require("../config/db.config.js");
const Sequelize = require("sequelize");
// const sequelize = new Sequelize(dbConfig.DB, dbConfig.USER, dbConfig.PASSWORD, {
//   host: dbConfig.HOST,
//   dialect: dbConfig.dialect,
// //   operatorsAliases: false,
// });
const sequelize = new Sequelize(process.env.DATABASE_URL,{
  dialectOptions: {
    ssl: {
      require: true,
      rejectUnauthorized: false
    }
  }
})
const db = {};
db.Sequelize = Sequelize;
db.sequelize = sequelize;
db.notifications = require("./notifications.model.js")(sequelize, Sequelize);
module.exports = db;