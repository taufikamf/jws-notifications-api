module.exports = (sequelize, Sequelize) => {
    const Notifications = sequelize.define("notification", {
    id:{
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
    },
    accountId: {
        type: Sequelize.INTEGER
    },
    title: {
        type: Sequelize.STRING
    },
    emailTo: {
        type: Sequelize.STRING
    },
    isRead: {
        type: Sequelize.INTEGER
    },
    description: {
        type: Sequelize.STRING
    },
    type: {
        type: Sequelize.STRING
    },
    linkPath: {
        type: Sequelize.STRING
    },
    linkParams: {
        type: Sequelize.STRING
    }
    });
    return Notifications;
  };