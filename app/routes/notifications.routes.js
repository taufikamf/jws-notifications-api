module.exports = app => {
    const notifications = require("../controllers/notifications.controller.js");
    var router = require("express").Router();
    // Create a new Notification
    router.post("/", notifications.create);
    // Retrieve all Notifications
    router.get("/", notifications.findAll);
    router.get("/email", notifications.sendEmail);
    router.get("/search", notifications.searchData);
    // Retrieve a Notification based create date
    router.get("/filter", notifications.filterData);
    // Retrieve a single Notification with id
    router.get("/:id", notifications.findOne);
    // Update a Notification with id
    router.put("/:id", notifications.update);
    // Delete a Notification with id
    router.delete("/:id", notifications.delete);
    // Delete all Notification
    router.delete("/", notifications.deleteAll);
    app.use('/api/notifications', router);
  };