const { type } = require("express/lib/response");
const { Sequelize } = require("../models");
const db = require("../models");
const Notifications = db.notifications;
const Op = db.Sequelize.Op;
const nodemailer = require("nodemailer");
// Create and Save a new Notifications
exports.create = (req, res) => {
  // Validate request
  if (!req.body.accountId) {
    res.status(400).send({
      message: "Content can not be empty!"
    });
    return;
  }
  // Create a Notifications
  const notifications = {
    accountId: req.body.accountId,
    title: req.body.title,
    emailTo: req.body.emailTo,
    isRead: req.body.isRead,
    description: req.body.description,
    type: req.body.type,
    linkPath: req.body.linkPath,
    linkParams: req.body.linkParams,
  };
  // Save Notifications in the database
  Notifications.create(notifications)
    .then(data => {
      res.send(data);
    })
    .catch(err => {
      res.status(500).send({
        message:
          err.message || "Some error occurred while creating the Notifications."
      });
    });
};
// Retrieve all Notifications from the database.
exports.findAll = (req, res) => {
    // const account_id = req.query.account_id;
    // var condition = account_id ? { account_id: { [Op.iLike]: `%${account_id}%` } } : null;
    Notifications.findAll({
      order : [["createdAt", 'ASC']]
    })
      .then(data => {
        // res.send(data);
        res.send(data)
      })
      .catch(err => {
        res.status(500).send({
          message:
            err.message || "Some error occurred while retrieving notifications."
        });
      });
};
// Find a single Notifications with an id
exports.findOne = (req, res) => {
    const id = req.params.id;
    Notifications.findByPk(id)
      .then(data => {
        if (data) {
          res.send(data);
        } else {
          res.status(404).send({
            message: `Cannot find Notifications with id=${id}.`
          });
        }
      })
      .catch(err => {
        res.status(500).send({
          message: "Error retrieving Notifications with aoskoaks=" + id
        });
      });
};
// Update a Notifications by the id in the request
exports.update = (req, res) => {
    const id = req.params.id;
    Notifications.update(req.body, {
      where: { id: id }
    })
      .then(num => {
        if (num == 1) {
          res.send({
            message: "Notifications was updated successfully."
          });
        } else {
          res.send({
            message: `Cannot update Notifications with id=${id}. Maybe Notifications was not found or req.body is empty!`
          });
        }
      })
      .catch(err => {
        res.status(500).send({
          message: "Error updating Notifications with id=" + id
        });
      });
};
// Delete a Notifications with the specified id in the request
exports.delete = (req, res) => {
    const id = req.params.id;
  Notifications.destroy({
    where: { id: id }
  })
    .then(num => {
      if (num == 1) {
        res.send({
          message: "Notifications was deleted successfully!"
        });
      } else {
        res.send({
          message: `Cannot delete Notifications with id=${id}. Maybe Notifications was not found!`
        });
      }
    })
    .catch(err => {
      res.status(500).send({
        message: "Could not delete Notifications with id=" + id
      });
    });
};
// Delete all Notifications from the database.
exports.deleteAll = (req, res) => {
    Notifications.destroy({
        where: {},
        truncate: false
      })
        .then(nums => {
          res.send({ message: `${nums} Notifications were deleted successfully!` });
        })
        .catch(err => {
          res.status(500).send({
            message:
              err.message || "Some error occurred while removing all notifications."
          });
        });
};
// Find all searched data
exports.searchData = (req, res) => {
  const {search} = req.query;
  if(!search){
    Notifications.findAll({
      order : [["createdAt", 'ASC']]
    })
      .then(data => {
        res.send(data);
      })
  }
  Notifications.findAll({ where: { [Op.or]:[
    { title: {[Op.iLike]: `%${search}%`} },
    { emailTo: {[Op.iLike]: `%${search}%`}},
    { description: {[Op.iLike]: `%${search}%`}},
    { type: {[Op.iLike]: `%${search}%`}},
    { linkPath: {[Op.iLike]: `%${search}%`}},
    { linkParams: {[Op.iLike]: `%${search}%`}},
  ]}})
    .then(data => {
      res.send(data);
    })
    .catch(err => {
      res.status(500).send({
        message:
          err.message || "Some error occured while retrieving data."
      });
    });
};
// //Filter all data
exports.filterData = (req, res) => {
  const {startDate, endDate} = req.query
  const isoDateStart = new Date(startDate)
  const isoDateEnd = new Date(endDate)
  // const date = isoDate.toISOString().substring(0, 10);
  // const date2 = new Date(date) 
  console.log(isoDateStart)
  Notifications.findAll({ where: { createdAt: {[Op.between]: [isoDateStart, isoDateEnd]} } })
    .then(data => {
      res.send(data);
    })
    .catch(err => {
      res.status(500).send({
        message:
          err.message || "Some error occurred while retrieving notifications."
      });
    });
};
exports.sendEmail = (req, res) => {
  const {accountId} = req.query;
  Notifications.findAll({ where: { accountId: accountId } })
    .then(data => {
      if (data) {
        let title = (data[0].title);
        let description = (data[0].description);
        let emailTo = (data[0].emailTo);
        var transporter = nodemailer.createTransport({
          host: process.env.MAILER_HOST,
          port: process.env.MAILER_PORT,
          secure: process.env.MAILER_SECURE,
          auth : {
              user : process.env.MAILER_USER,
              pass : process.env.MAILER_PASS
          }
      });
        var mailOptions = {
          from: 'Smiling West Java',
          to: emailTo,
          subject: title,
          text: description
        };
        
        transporter.sendMail(mailOptions, function(error, info){
          if (error) {
            res.status(404).send({
              message: `Cannot send email with title: ${title}.`
            });
          } else {
            res.send({
              message: `Email sent successfully`
            });
          }
        });
      } else {
        res.status(404).send({
          message: `Cannot find data with id=${accountId}.`
        });
      }
    })
    .catch(err => {
      res.status(500).send({
        message: "Error retrieving data with id=" + accountId
      });
    });
};